package ru.bakhtiyarov.tm.exception;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
