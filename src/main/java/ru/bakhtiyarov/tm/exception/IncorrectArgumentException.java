package ru.bakhtiyarov.tm.exception;

public class IncorrectArgumentException extends RuntimeException{

    public IncorrectArgumentException(final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

}
